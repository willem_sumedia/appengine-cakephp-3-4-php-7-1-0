# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## GOOGLE CLOUD PLATFORM

See also http://rricketts.com/google-cloud-platform-cakephp-3-x-3-4/

### Using Google Cloud Shell

Open the Google Cloud Shell on the Web console (https://console.cloud.google.com) using the [>_] button.

You will be prompted as follows:

```javascript
Welcome to Cloud Shell! Type "help" to get started.
willem@verkeerslicht-165607:~$
```

where 'willem' is the user with which you are logged in, and 'verkeerslicht-165607' is the project name.

Start in your user's src directory on the google cloud console

```javascript
cd ~/src
```

### Install Composer

```javascript
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
```

Update your composer requirements:

```javascript
composer require "cakephp/cakephp:3.4.*" --ignore-platform-reqs
composer require "php:7.1.*" --ignore-platform-reqs
```

2. Clone the sample code

Use Cloud Shell to clone and navigate to the code. The application code is cloned from your project repository to the Cloud Shell.

In Cloud Shell enter:

```javascript
CLONEDTODIR=~/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app
```

Clone a sample repository:

```javascript
git clone https://willem_sumedia@bitbucket.org/willem_sumedia/appengine-cakephp-3-4-starter-app.git $CLONEDTODIR
```

You will be prompted with:

```javascript
Cloning into '/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app'...
remote: Counting objects: 53, done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 53 (delta 15), reused 0 (delta 0)
Unpacking objects: 100% (53/53), done.
```

Switch to the tutorial directory:

```javascript
cd $CLONEDTODIR
```

Or, if you have cloned into the directory before, pull the sample repository instead:

```javascript
git pull
```

You will be prompted with:

```javascript
willem@verkeerslicht-165607:~/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app$
```

### Install required packages with composer

Before using composer, make sure all required libraries are enabled in php.ini.

On Google Cloud Platform the php.ini file is located at '/etc/php5/cli/php.ini'.

It should have the following libraries enabled in php.ini:

#### intl

Installing on Linux (Ubuntu), run the following commands:

```javascript
sudo apt-get install php5-intl
```

You can also check what extensions are compiled in by running:

```javascript
php -m
```

Enable these settings in php.ini; ```sudo nano php.ini```

```javascript
[intl]
intl.default_locale = en_utf8
; This directive allows you to produce PHP errors when some error
; happens within intl functions. The value is the level of the error produced.
; Default is 0, which does not produce any errors.
intl.error_level = E_WARNING
intl.use_exceptions = 0
```

#### Run composer

Run the following command from within the cloned repository directory:

```javascript
composer install
```

This will install all required 'vendor' packages in the 'vendor' directory.

You will be prompted:

```javascript
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Package operations: 35 installs, 0 updates, 0 removals
  - Installing cakephp/plugin-installer (1.0.0): Downloading (100%)         
  - Installing aura/intl (3.0.0): Downloading (100%)         
  - Installing cakephp/chronos (1.1.1): Downloading (100%)         
  - Installing symfony/yaml (v3.2.7): Downloading (100%)         
  - Installing symfony/polyfill-mbstring (v1.3.0): Downloading (100%)         
  - Installing psr/log (1.0.2): Downloading (100%)         
  - Installing symfony/debug (v3.2.7): Downloading (100%)         
  - Installing symfony/console (v3.2.7): Downloading (100%)         
  - Installing symfony/filesystem (v3.2.7): Downloading (100%)         
  - Installing symfony/config (v3.2.7): Downloading (100%)         
  - Installing robmorgan/phinx (v0.6.5): Downloading (100%)         
  - Installing psr/http-message (1.0.1): Downloading (100%)         
  - Installing zendframework/zend-diactoros (1.4.0): Downloading (100%)         
  - Installing cakephp/cakephp (3.4.5): Downloading (100%)         
  - Installing cakephp/migrations (1.6.7): Downloading (100%)         
  - Installing mobiledetect/mobiledetectlib (2.8.25): Downloading (100%)         
  - Installing cakephp/bake (1.3.3): Downloading (100%)         
  - Installing jdorn/sql-formatter (v1.2.17): Downloading (100%)         
  - Installing symfony/process (v3.2.7): Downloading (100%)         
  - Installing symfony/finder (v3.2.7): Downloading (100%)         
  - Installing seld/phar-utils (1.0.1): Downloading (100%)         
  - Installing seld/jsonlint (1.6.0): Downloading (100%)         
  - Installing seld/cli-prompt (1.0.3): Downloading (100%)         
  - Installing justinrainbow/json-schema (5.2.0): Downloading (100%)         
  - Installing composer/spdx-licenses (1.1.6): Downloading (100%)         
  - Installing composer/semver (1.4.2): Downloading (100%)         
  - Installing composer/ca-bundle (1.0.7): Downloading (100%)         
  - Installing composer/composer (1.4.1): Downloading (100%)         
  - Installing cakephp/debug_kit (3.9.3): Downloading (100%)         
  - Installing jakub-onderka/php-console-color (0.1): Downloading (100%)         
  - Installing symfony/var-dumper (v3.2.7): Downloading (100%)         
  - Installing nikic/php-parser (v3.0.5): Downloading (100%)         
  - Installing jakub-onderka/php-console-highlighter (v0.3.2): Downloading (100%)         
  - Installing dnoegel/php-xdg-base-dir (0.1): Downloading (100%)         
  - Installing psy/psysh (v0.8.3): Downloading (100%)         
symfony/console suggests installing symfony/event-dispatcher ()
cakephp/cakephp suggests installing lib-ICU (The intl PHP library, to use Text::transliterate() or Text::slug())
cakephp/debug_kit suggests installing ext-sqlite (DebugKit needs to store panel data in a database. SQLite is simple and easy to use
.)
symfony/var-dumper suggests installing ext-symfony_debug ()
psy/psysh suggests installing ext-pdo-sqlite (The doc command requires SQLite to work.)
psy/psysh suggests installing ext-readline (Enables support for arrow-key history navigation, and showing and manipulating command h
istory.)
psy/psysh suggests installing hoa/console (A pure PHP readline implementation. You'll want this if your PHP install doesn't already 
support readline or libedit.)
Generating autoload files
> Cake\Composer\Installer\PluginInstaller::postAutoloadDump
> App\Console\Installer::postInstall
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/logs` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache/models` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache/persistent` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache/views` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/sessions` directory
Created `/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/tests` directory
Set Folder Permissions ? (Default to Y) [Y,n]? 
```

Confirm by typing: Y (followed by ENTER)

```javascript
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache/models
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache/persistent
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/cache/views
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/sessions
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp/tests
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/tmp
Permissions set on /home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/logs
No Security.salt placeholder to replace.
```

### Deploying the app to the App Engine flexible environment

NOTE: Make sure you have completed the steps of the previous sections; "Using Google Cloud Shell" and "Install required packages with composer".

1. Deploy the sample app, by typing in the online Cloud Shell console :

```javascript
gcloud app deploy --project verkeerslicht-165607
```

You will be prompted with:

```javascript
You are about to deploy the following services:
 - verkeerslicht-165607/default/20170425t104700 (from [/home/willem/src/verkeerslicht-165607/appengine-cakephp-3-4-starter-app/app.yaml])
     Deploying to URL: [https://verkeerslicht-165607.appspot.com]

Do you want to continue (Y/n)?
```

Type: Y (followed by ENTER)

You will be prompted:

```javascript
...
DONE
----------------------------------------------------------------------------------------------------------------------------------

Updating service [default]...\Stopping version [verkeerslicht-165607/default/20170424t153456].                                    
Updating service [default]...|Sent request to stop version [verkeerslicht-165607/default/20170424t153456]. This operation may take some time to complete. If you would like to verify that it succeeded, run:
  $ gcloud app versions describe -s default 20170424t153456
until it shows that the version has stopped.
Updating service [default]...done.                                                                                                
Deployed service [default] to [https://verkeerslicht-165607.appspot.com]

You can stream logs from the command line by running:
  $ gcloud app logs tail -s default

To view your application in the web browser run:
  $ gcloud app browse
```

Type:

```javascript
gcloud app browse
```
You will be prompted:

```javascript
Opening [https://verkeerslicht-165607.appspot.com] in a new tab in your default browser.
https://verkeerslicht-165607.appspot.com
```

Open a web browser and browse to https://verkeerslicht-165607.appspot.com